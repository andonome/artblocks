This repo does everything NFTs are meant to do using nothing but git + lfs.

It doesn't damage the environment, but it *can* show ownership via the tried and tested method of gpg keys and a ring of trust.
People just need to upload their art, sign the commit, and you're done.

|                          | NFTs                                          | Git                                 |
|:-------------------------|:----------------------------------------------|:------------------------------------|
| Verify art?              | No, just gives a changeable link              | Yes, with sha256 sums               |
| Verify owner?            | No, just provides links                       | Yes, with gpg keys                  |
| Running cost?            | Loads - proof of work required                | None - just sits there              |


## How to Add Art

Just commit it with lfs installed, and add the owner as metadata, or make a file stating the owner's name, with the owner's public GPG key.

Alternatively, commit CC works in a directory which lists that licence.

# NAQ (Never Asked Questions)

> What proves you own the art?

A ring of trust.
You can claim that you own anything on your own copy, but you should only accept pull requests from people you trust.

> Once I pay money, what proves that the artist will transfer ownership?

Public ledgers.
Any artist can place a price, and the first person to transfer the money to a public wallet owns the piece.
If you can prove ownership of the sending wallet, this shows you've paid, so disagreements can be sorted instantly, or change of ownership could be automated with a bash script.

Or just message the artist and pay in the normal way.

> Won't the repository become huge with lots of art?

No!
Git lfs just uses shasums to verify images, so even a 500MB photograph can be verified here with a single line of text.

> What do artists without gpg keys do?

Ask your local techie to verify for an artist with their email and name.
The person committing is listed separately from the author in git.

`git commit --author="HR Geiger <geiger@email.swiss>" -m "image owned by Aliens franchize"`

> What keeps the network going?

You can't push until you pull.

> What if I sell art to someone not involved in the project?

Just delete your piece, and perhaps make a note which unambiguously specifies the new owner.

> Who needs this?

Nobody.
People have been making and selling art forever, without a blockchain.

> Is this a joke?

Yes, but not as much as the original NFTs.
